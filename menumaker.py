#!/usr/bin/env python3

# Copyright (c) 2012 Benjamin Althues <benjamin@babab.nl>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

__author__ = 'Benjamin Althues'
__copyright__ = 'Copyright (C) 2012 Benjamin Althues'
__docformat__ = 'restructuredtext'
__version__ = '0.1'
__version_info__ = (0, 1, 0, 'alpha', 0)
versionStr = 'MenuMaker v%s' % __version__

long_description = '''
A work in progress. This is just a setup of the project.
'''

import curses
import subprocess
import sys


class MenuMaker(object):
    menuItems = {}
    screen = None
    showHelp = True
    title = versionStr

    def __init__(self, menuItems, startLoop=False):
        self.menuItems = menuItems

        if startLoop:
            self.mainLoop()

    def setHelp(self, help=True):
        self.showHelp = help
        return self

    def setTitle(self, title):
        self.title = title
        return self

    @staticmethod
    def getOption(char):
        if char == ord('1') or char == ord('a'):
            return 1
        elif char == ord('2') or char == ord('b'):
            return 2
        elif char == ord('3') or char == ord('c'):
            return 3
        elif char == ord('4') or char == ord('d'):
            return 4
        elif char == ord('5') or char == ord('e'):
            return 5
        elif char == ord('6') or char == ord('f'):
            return 6
        elif char == ord('7') or char == ord('g'):
            return 7
        elif char == ord('8') or char == ord('h'):
            return 8
        elif char == ord('9') or char == ord('i'):
            return 9
        else:
            return False

    def screenInit(self):
        self.screen = curses.initscr()
        curses.noecho()
        curses.cbreak()
        curses.curs_set(0)

    def screenClose(self):
        self.screen.erase()
        curses.nocbreak()
        curses.echo()
        curses.endwin()

    def mainLoop(self):
        self.screenInit()
        choice = False
        command = None
        while True:
            self.screen.addstr(0, 0, str(self.title), curses.A_BOLD)

            row = 2
            if self.showHelp:
                row = 3
                self.screen.addstr(1, 0, "Use 'j' and 'k' to select an item | "
                                   "Press 'q' to quit")

            optn = 1
            for opt, cmd in self.menuItems.items():
                if choice == optn:
                    self.screen.addstr(row, 1, '* ' + str(optn) + ': ' + opt,
                                       curses.A_BOLD)
                    command = cmd
                else:
                    self.screen.addstr(row, 3, str(optn) + ': ' + opt)
                optn += 1
                row += 1

            if self.showHelp and choice:
                self.screen.addstr(row + 1, 0, "Press 'r' to run '"
                                   + str(command)
                                   + "' | Press 'R' to add arguments")
            self.screen.refresh()

            char = self.screen.getch()
            if char == ord('q'):
                self.screen.erase()
                break
            elif char == ord('r'):
                self.screenClose()
                sys.exit(subprocess.call(command.strip().split(' ')))
            elif char == ord('R'):
                self.screenClose()
                command = command.strip()
                command += ' ' + input('$ ' + command + ' ').strip()
                sys.exit(subprocess.call(command.split(' ')))
            elif char == ord('H'):
                self.showHelp = False if self.showHelp else True
            elif char == ord('j'):
                if choice == len(self.menuItems):
                    choice = 1
                else:
                    choice += 1
            elif char == ord('k'):
                if choice <= 1:
                    choice = len(self.menuItems)
                else:
                    choice -= 1
            elif char == ord('g'):
                choice = 1
            elif char == ord('G'):
                choice = len(self.menuItems)
            else:
                choice = self.getOption(char)

                if choice and choice > len(self.menuItems):
                    choice = False

            self.screen.erase()
        self.screenClose()

if __name__ == '__main__':
    menu = {'Show environment vars': 'env',
            'Echo input': 'echo',
            'DisPass': 'dispass',
            'Long listing': 'ls -l'}
    if len(sys.argv) > 1:
        (MenuMaker(menu).setTitle(' '.join(sys.argv[1:]))
                        .setHelp(False).mainLoop())
    else:
        MenuMaker(menu, startLoop=True)
